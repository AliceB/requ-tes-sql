import sqlite3
from enum import Enum

class Type_SQL(Enum):
    NONE = None
    VARCHAR = str
    INTEGER = int
    FLOAT = float
    BOOL = bool

class Table:
    '''
        conn : connexion à la base de donnée
        nom : nom de la table
        structure : liste de dictionnaire de nom de colonne et de type
    '''
    def __init__(self, nom_database, connexion, nom_table : str):
        self.db = nom_database
        self.conn = connexion
        self.nom = nom_table
        self.structure = {}

        if self.conn is not None :
            cur = self.conn.cursor()
            sql = ""

            if self.db == "sqlite3":
                sql = str("PRAGMA table_info(" + self.nom +")")
            elif self.db == "postgresql":
                sql = str(f"""
                            SELECT ordinal_position,column_name, data_type 
                            FROM information_schema.columns
                            WHERE table_name = '{self.nom}'
                        """)
            cur.execute(sql)
            info = cur.fetchall()
            list_info = {}

            for row in info:
                list_info[row[1]] = row[2]

            self.structure = list_info
   
    def insert_data(self, donnee : list):
        '''
            Liste de dictionaire de nom de colonne (str) et de valeurs.
            Permet d'inserer des données
        '''

        for iquery in donnee:

            str_col = ""
            str_val = ""
            wr_cols = []
            for key, value in iquery.items():
                try:
                    type_col = self.structure[key]
                    
                    str_col += key + ", "

                    if type_col.lower() in ["integer", "float", "int"]:
                        str_val += str(value) + ", "
                    else:
                        str_val += "'" + str(value) + "', "

                except KeyError as e:
                    wr_cols.append(key)

            if len(wr_cols) != 0:
                print(f"Les colonnes {wr_cols} n'existent pas dans la table {self.nom}")
                #erreur
            else:
                str_col = str_col[0:-2]
                str_val = str_val[0:-2]
                sql = f"""
                        INSERT INTO {self.nom} ({str_col})
                        VALUES ({str_val});
                    """
            
                curs = self.conn.cursor()
                #print(sql)
                curs.execute(sql) 
        self.conn.commit()  

    def read_data(self, colonne=[], condition={})-> list:
            '''
                Liste de noms (str) et un dictionnaire de nom de colonne (str) et valeur de colonne.
                Selectionne les colonnes spécifiées, sinon selectionne tout s'il n'y a pas de champs renseigné.
            '''

            if self.conn is not None :
                cur = self.conn.cursor()
                str_noms = ""
                str_valeur = ""
                str_condition = "" 
                struct_type = Type_SQL.NONE
                sql = ""

                for data in condition:
                    if self.structure[data][0:7] == "VARCHAR":
                        struct_type = Type_SQL.VARCHAR
                        if type(condition[data]) == struct_type.value:
                            str_valeur = "'" + str(condition[data]) + "'"
                    elif self.structure[data] == "INTEGER":
                        struct_type = Type_SQL.INTEGER
                        if type(condition[data]) == struct_type.value:
                            str_valeur = str(condition[data])
                    elif self.structure[data] == "FLOAT":
                        struct_type = Type_SQL.FLOAT
                        if type(condition[data]) == struct_type.value:
                            str_valeur = str(condition[data])
                    else:
                        str_valeur = str(condition[data])

                    str_condition = str_condition + data+ " = " + str_valeur + " AND "  

                if colonne != []:
                    if len(colonne) > 0:
                        for nom in colonne:
                            str_noms = str_noms + nom + ", " 
                        
                        if condition != {}:
                            str_noms = str_noms[0:-2]           
                            str_condition = str_condition[0:-5]
                            sql = str("SELECT " +str_noms + " FROM " + self.nom + " WHERE " +str_condition)
                    else:
                        str_noms = str_noms[0:-2]
                        sql = str("SELECT " +str_noms + " FROM " + self.nom)
                elif condition != {}:
                    str_condition = str_condition[0:-5]
                    sql = str("SELECT * FROM " + self.nom +" WHERE " +str_condition)
                else :
                    sql = str("SELECT * FROM " + self.nom)   
                cur.execute(sql)
                return cur.fetchall()
            else:
                print("La connexion n'existe pas")
            return []

    def update_data(self,condition : dict, modification : dict):
        '''
            Dictionnaire "condition" : nom de colonnes (str) et valeur de colonnes
            Dictionnaire "modification" : nom de colonnes (str) et valeur de colonnes
            Permet de remplacer des données via une condition          
        '''

        str_con = ""
        str_mod = ""
        wr_cons = []
        wr_mods = []

        for key, value in condition.items():
            try:
                type_con = self.structure[key]

                str_con += key + " =  "

                if type_con.lower() in ["integer", "float", "int"]:
                    str_con += str(value) + " AND "
                else:
                    str_con += "'" + str(value) + "' AND "

            except KeyError as e:
                wr_cons.append(key)

        if len(wr_cons) == 0:
            str_con = str_con[0:-4]

            for key, value in modification.items():
                try:
                    type_mod = self.structure[key]
                    
                    str_mod += key + " = "

                    if type_mod.lower() in ["integer", "float", "int"]:
                        str_mod += str(value) + ", "
                    else:
                        str_mod += "'" + str(value) + "', "

                except KeyError as e:
                    wr_mods.append(key)


            if len(wr_mods) == 0:
                str_mod = str_mod[0:-2]

                sql = f"""
                        UPDATE {self.nom}
                        SET {str_mod}
                        WHERE {str_con};
                    """
                #print(sql)

                curs = self.conn.cursor()
                curs.execute(sql)
                self.conn.commit()

            else:
                print(f"Les colonnes {wr_mods} n'existent pas dans la table {self.nom}.")
                #erreur

        else:
            print(f"Les colonnes {wr_cons} n'existent pas dans la table {self.nom}.")
            #erreur

        pass

    def delete_data(self, condition : list): #colonne #condition
        '''
            Liste de Dictionaire de conditions. 
            Permet de supprimer des lignes de données
        '''

        sql = ""
        str_valeur = ""
        valeur = None
        
        if self.conn is not None :
            cur = self.conn.cursor()
            for dico in condition:
                if type(dico) == type(dict()):
                    str_condition = ""
                    for key in dico:
                        #print(dico[key])
                        valeur = dico[key]

                        if self.structure[key][0:7] == "VARCHAR":
                            struct_type = Type_SQL.VARCHAR
                            if type(valeur) == struct_type.value:
                                str_valeur = "'" + str(valeur) + "'"
                        elif self.structure[key] == "INTEGER":
                            struct_type = Type_SQL.INTEGER
                            if type(valeur) == struct_type.value:
                                str_valeur = str(valeur)
                        elif self.structure[key] == "FLOAT":
                            struct_type = Type_SQL.FLOAT
                            if type(valeur) == struct_type.value:
                                str_valeur = str(valeur)
                        else:
                            str_valeur = str(valeur)

                        str_condition = str_condition + key + " = " + str_valeur + " AND "

                    str_condition = str_condition[0:-5]
                    sql = str("DELETE FROM " + self.nom + " WHERE " + str_condition)
                    cur.execute(sql)
                else:
                    print("Une liste de dictionnaire est demandé !")
                    