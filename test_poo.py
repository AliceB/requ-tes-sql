from SqliteDB import SqliteDB
from Table import Table
from PostgreDB import PostgreDB

new_db = SqliteDB("projet_DB")

#new_db = PostgreDB("projet_DB2")

new_table = new_db.create_table("voiture",
                    {"marque" : "VARCHAR",
                     "modele" : "VARCHAR",
                     "annee" : "INT"}
                    )


new_table.insert_data([
                        {"marque" : "Ford",
                        "modele" : "Mustang",
                        "annee" : 1964},
                        {"marque" : "Fiat",
                        "modele" : "Punto",
                        "annee" : 1995},
                        {"marque" : "Renault",
                        "modele" : "R5",
                        "annee" : 1988}
                    ])

print(new_table.read_data())

print(new_table.read_data(condition={"annee": 1964}))

print(new_table.read_data(colonne=["marque","modele"],condition={"annee": 1964}))


new_table.update_data({"annee": 1964}, {"marque" : "Fiat",
                      "modele" : "Punto",
                      "annee" : 1995})

print(new_table.read_data())

new_table.delete_data([{"annee": 1964}])

print(new_table.read_data())
