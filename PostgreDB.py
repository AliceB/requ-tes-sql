from Table import Table
from DataBase import DataBase
# Load libraries
from sqlalchemy import create_engine
from sqlalchemy_utils import create_database, database_exists, drop_database
import psycopg2


class PostgreDB (DataBase) :
    '''
    - conn: connexion à la base
    - nom: str - nom de la base
    - l_tables: liste des Table de la base
    - sgbd: str -
    '''

    def __init__(self, nom_base: str, host="localhost", user="postgres",passw="", port="5432"):
        self.sgbd = "postgresql"
        super().__init__(nom_base)


        self.conn = psycopg2.connect(f"dbname={nom_base} user={user} host={host} port={port}")
        
        
        if self.conn is not None:
            cur = self.conn.cursor()


            sql_schema = "SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname = 'public';"

            cur.execute(sql_schema)
            schema = cur.fetchall()
            
            if len(schema) > 0:
                self.l_tables = []

                for i_table in schema:
                    self.l_tables.append(Table(self.sgbd, self.conn, i_table[0]))                
                        
                        
    def __del__():
        super().__del__()




    def create_table(self, nom_table: str, columns: dict):
        '''
        - nom_table: str - nom de la table à créer
        - columns: dict au format "nom de la colonne": "type de la colonne" 
        '''

        return super().create_table(nom_table,columns)
