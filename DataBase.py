from Table import Table
import SqliteDB
import sqlite3
from sqlite3 import Error as S3Error
from time import sleep

class DataBase:
    '''
    - conn: connexion à la base
    - nom: str - nom de la base
    - l_tables: liste des Table de la base
    - sgbd: str -
    '''

    def __init__(self, nom_base: str):
        self.nom = nom_base
        self.l_tables = []
        self.conn = None
                        
                        
    def __del__(self):
        if self.conn is not None:
            for itable in self.l_tables:
                del itable
            self.conn.close()




    def create_table(self, nom_table: str, columns: dict):
        '''
        - nom_table: str - nom de la table à créer
        - columns: dict au format "nom de la colonne": "type de la colonne" 
        '''

        table = None       
        if self.conn is not None:
            cur = self.conn.cursor()
            key = list(columns.keys())
            value = list(columns.values())
            
            sql_create = "CREATE TABLE IF NOT EXISTS"+" "+ nom_table + " ("
            
            for i in range(0,len(key)):
                sql_create = sql_create + str(key[i])+" "+str(value[i])+ ","

            sql_create = sql_create [:-1] + ");"

            cur.execute(sql_create)
            self.conn.commit()

            table = Table(self.sgbd, self.conn, nom_table)
            self.l_tables.append(table)

            #print(sql_create)


        return table
