from Table import Table
from DataBase import DataBase
import sqlite3
from sqlite3 import Error as S3Error

class SqliteDB (DataBase) :
    '''
    - conn: connexion à la base
    - nom: str - nom de la base
    - l_tables: liste des Table de la base
    - sgbd: str -
    '''

    def __init__(self, nom_base: str):
        self.sgbd = "sqlite3"
        super().__init__(nom_base)

        try:
            self.conn = sqlite3.connect(nom_base)
            #   print(sqlite3.version)
        except S3Error as e:
            print(e)
        
        if self.conn is not None:
            cur = self.conn.cursor()
            sql_schema = "SELECT name FROM sqlite_master WHERE type='table';"

            cur.execute(sql_schema)
            schema = cur.fetchall()
            
            if len(schema) > 0:
                self.l_tables = []

                for i_table in schema:
                    self.l_tables.append(Table(self.sgbd, self.conn, i_table[0]))                
                        
                        
    def __del__():
        super().__del__()




    def create_table(self, nom_table: str, columns: dict):
        '''
        - nom_table: str - nom de la table à créer
        - columns: dict au format "nom de la colonne": "type de la colonne" 
        '''

        return super().create_table(nom_table,columns)
